"use strict";

const path = require('path');

const fs = require('fs');
const { Command } = require('commander');
const program = new Command();
const chalk = require('chalk');

const imgFolder = 'public/img';
const jsonFolder = 'src/assets/json'

program
  .name('generate')
  .action(() => {
    console.log(chalk.white('Genator started'));
    generate().catch((error) => {
        console.log(chalk.red(error));
    });
  });

program.parse();


async function generate() {
    const dir = await fs.promises.opendir(imgFolder)
    for await (const dirent of dir) {
        if (dirent.isDirectory()) {
            LoopImages(dirent.name);
        }
    }
    console.log(chalk.white('Genator ended'));
}

async function LoopImages(folder) {
    console.log(chalk.cyan('Starting: ' + folder));
    let traitList = {};

    const dir = await fs.promises.opendir(imgFolder + '/' + folder)
    for await (const dirent of dir) {
        console.log(chalk.white(dirent.name));
        let name = path.parse(dirent.name).name;
        let traitname = name.split(' ').join('_');

        let json = {
            name: traitname,
            img: `img/${folder}/${dirent.name}`,
        };
    
        traitList[traitname] = json;
    }
    fs.writeFileSync(
        `${jsonFolder}/${folder}.json`,
        JSON.stringify(traitList, null, 2)
    );

    console.log(chalk.green('Completed: ' + folder));
}