import VueRouter from 'vue-router';
import routes from './../routes';

// Set up the Router
const router = new VueRouter
({
    mode: 'history',
    routes,
    scrollBehavior () {
        return { x: 0, y: 0 }
    }
});

// Router filters - run before a page is loaded.
router.beforeEach((to, from, next) => {
    document.title = '1337 Skulls - ' + to.meta.title;
    next();
});

export default router;
