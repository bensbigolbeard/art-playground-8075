module.exports = {
  root: true,
  env: {
      node: true
  },
  parser: 'vue-eslint-parser',
  plugins: [
      'es-beautifier'
  ],
  extends: [
      'eslint:recommended',
      'plugin:vue/recommended',
  ],
  rules: {
      'comma-dangle': ['off'],
      'linebreak-style': 'off',
      'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
      'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
      'quotes': ['warn', 'single', { 'allowTemplateLiterals': true }],
      'vue/attribute-hyphenation': ['off'],
      'vue/html-closing-bracket-newline': ['off'],
      'vue/html-indent': ['warn', 4],
      'vue/max-attributes-per-line': ['error', {
          'singleline': 5,
          'multiline': {
              'max': 1,
              'allowFirstLine': false
          }
      }],
      'vue/multiline-html-element-content-newline': ['off'],
      'vue/name-property-casing': ['warn', 'kebab-case'],
      'vue/no-unused-vars': ['warn'],
      'vue/singleline-html-element-content-newline': ['off'],
  },
  globals: {
      $nuxt: true,
      '_': true,
      'Auth': true,
      'Vue': true,
  },
  parserOptions: {
      parser: 'babel-eslint'
  }
};
